// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TDSState_Effect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDSState_Effect : public UObject
{
	GENERATED_BODY()
public:
	virtual bool InitObject(AActor* Actor,FName NameBonesHit);
	virtual void DestroyObject();
	
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting")
	bool bIsStackabale = false;
	


	AActor* myActor = nullptr;
	
};

UCLASS()
class TDS_API UTDSState_Effect_ExecuteOnce : public UTDSState_Effect
{
	GENERATED_BODY()
public:
	bool InitObject(AActor* Actor,FName NameBonesHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting Execute Once")
	float Power = 20.0f;
};

UCLASS()
class TDS_API UTDSState_Effect_ExecuteTimer : public UTDSState_Effect
{
	GENERATED_BODY()
public:
	 bool InitObject(AActor* Actor,FName NameBonesHit) override;
	 void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting Execute Once")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting Execute Once")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting Execute Once")
	float RateTimer = 1.0f;
	
	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting Execute Once")
	UParticleSystem* ParticalEffect = nullptr;
	UParticleSystemComponent* ParticleSystemComponent = nullptr;
	
};

UCLASS()
class TDS_API UTDSState_Effect_Stan : public UTDSState_Effect
{
	GENERATED_BODY()
public:

	bool InitObject(AActor* Actor,FName NameBonesHit) override;
	void DestroyObject() override;
	
	
	FTimerHandle TimerHandle_EffectTimer;

	void ChangeCharacterInput(bool Status);
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting")
	UParticleSystem* ParticalEffect = nullptr;
	UParticleSystemComponent* ParticleSystemComponent = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting")
	UAnimMontage* AnimMontage = nullptr;
	
};

UCLASS()
class TDS_API UTDSState_Effect_GodMode : public UTDSState_Effect
{
	GENERATED_BODY()
public:

	bool InitObject(AActor* Actor,FName NameBonesHit) override;
	void DestroyObject() override;
	
	
	FTimerHandle TimerHandle_EffectTimer;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting")
	UParticleSystem* ParticalEffect = nullptr;
	UParticleSystemComponent* ParticleSystemComponent = nullptr;
	
};