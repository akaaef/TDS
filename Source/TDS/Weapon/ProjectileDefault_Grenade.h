// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	UFUNCTION()
	void Explose();

	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = Debug)
	bool DebugRadius = false;

	UFUNCTION(NetMulticast,Reliable)
	void ExploseFX_Multicast(UParticleSystem* FX);
	UFUNCTION(NetMulticast,Reliable)
	void ExploseSound_Multicast(USoundBase* Sound);
};
