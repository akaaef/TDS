// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSState_Effect.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "TDS/Interface/TDSIGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Interface/TDSIGameActor.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS/Character/TDSCharacter.h"
#include "TDS/Character/TDSHealthComponent.h"

bool UTDSState_Effect::InitObject(AActor* Actor,FName NameBonesHit)
{
	myActor = Actor;

	ITDSIGameActor* myInterface = Cast<ITDSIGameActor>(myActor);
	if(myInterface)
	{
		myInterface->AddEffect(this);
	}
	
	return true;
}

void UTDSState_Effect::DestroyObject()
{
	ITDSIGameActor* myInterface = Cast<ITDSIGameActor>(myActor);
	if(myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	
	myActor = nullptr;
	if(this && IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDSState_Effect_ExecuteOnce::InitObject(AActor* Actor,FName NameBonesHit)
{
	Super::InitObject(Actor,NameBonesHit);
	ExecuteOnce();
	return true;
}

void UTDSState_Effect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDSState_Effect_ExecuteOnce::ExecuteOnce()
{
	if(myActor)
	{
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if(myHealthComponent)
		{
			myHealthComponent->ChangeHealthValue(Power);
		}
	}
	
	
	DestroyObject();
}

bool UTDSState_Effect_ExecuteTimer::InitObject(AActor* Actor,FName NameBonesHit)
{
	Super::InitObject(Actor,NameBonesHit);
	
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer,this,&UTDSState_Effect_ExecuteTimer::DestroyObject,Timer,false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer,this,&UTDSState_Effect_ExecuteTimer::Execute,RateTimer,true);

	if(ParticalEffect)
	{
		FName NameBoneToAttached = NameBonesHit;
		FVector Loc = FVector(0);


		USceneComponent* myMash = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if(myMash)
		{
			ParticleSystemComponent = UGameplayStatics::SpawnEmitterAttached(ParticalEffect,myMash,NameBoneToAttached,Loc,FRotator::ZeroRotator,EAttachLocation::SnapToTarget,false);
		}
		else
		{
			ParticleSystemComponent = UGameplayStatics::SpawnEmitterAttached(ParticalEffect,myActor->GetRootComponent(),NameBoneToAttached,Loc,FRotator::ZeroRotator,EAttachLocation::SnapToTarget,false);
		}
		
	}
	return true;
}

void UTDSState_Effect_ExecuteTimer::DestroyObject()
{
	if(ParticleSystemComponent)
	{
		ParticleSystemComponent->DestroyComponent();
		ParticleSystemComponent = nullptr;
	}
	Super::DestroyObject();
}

void UTDSState_Effect_ExecuteTimer::Execute()
{
	ATDSCharacter* Character = Cast<ATDSCharacter>(myActor);
	
	if(myActor && Character)
	{
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		
		if(myHealthComponent && !Character->GodMode)
		{
			myHealthComponent->ChangeHealthValue(-Power);
		}
	}
}


bool UTDSState_Effect_Stan::InitObject(AActor* Actor,FName NameBonesHit)
{
	Super::InitObject(Actor,NameBonesHit);
	

	if(myActor)
	{
		ChangeCharacterInput(true);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer,this,&UTDSState_Effect_Stan::DestroyObject,3,false);
		if(ParticalEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0,0,100);
			ParticleSystemComponent = UGameplayStatics::SpawnEmitterAttached(ParticalEffect,myActor->GetRootComponent(),NameBoneToAttached,Loc,FRotator::ZeroRotator,EAttachLocation::SnapToTarget,false);
		}
		
	}
	return true;
}

void UTDSState_Effect_Stan::DestroyObject()
{
	
	if(myActor)
	{
		ChangeCharacterInput(false);

		ACharacter* Character = Cast<ACharacter>(myActor);
		if(Character)
		{
			Character->GetCharacterMovement()->MaxWalkSpeed = 450;
		}
	}
	
	if(ParticleSystemComponent)
	{
		ParticleSystemComponent->DestroyComponent();
		ParticleSystemComponent = nullptr;
	}
	Super::DestroyObject();
}


void UTDSState_Effect_Stan::ChangeCharacterInput(bool Status)
{
	if(AnimMontage)
	{
		ACharacter* Character = Cast<ACharacter>(myActor);

		if(Character && Status)
		{
			Character->PlayAnimMontage(AnimMontage);
			Character->GetCharacterMovement()->MaxWalkSpeed = 0;
			Character->GetCharacterMovement()->StopMovementImmediately();
			Character->DisableInput(Cast<APlayerController>(Character->GetController()));
		}
		if(Character && !Status)
		{
			Character->EnableInput(Cast<APlayerController>(Character->GetController()));
		}
	}
}

bool UTDSState_Effect_GodMode::InitObject(AActor* Actor,FName NameBonesHit)
{
	Super::InitObject(Actor,NameBonesHit);

	ATDSCharacter* Character = Cast<ATDSCharacter>(myActor);
	
	if(myActor && Character)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer,this,&UTDSState_Effect_GodMode::DestroyObject,15,false);

		Character->GodMode = true;
		
		if(ParticalEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);
			ParticleSystemComponent = UGameplayStatics::SpawnEmitterAttached(ParticalEffect,myActor->GetRootComponent(),NameBoneToAttached,Loc,FRotator::ZeroRotator,EAttachLocation::SnapToTarget,false);
		}
	}
	return true;
}

void UTDSState_Effect_GodMode::DestroyObject()
{
	ATDSCharacter* Character = Cast<ATDSCharacter>(myActor);
	
	if(Character)
	{
		Character->GodMode = false;
	}

	if(ParticleSystemComponent)
	{
		ParticleSystemComponent->DestroyComponent();
		ParticleSystemComponent = nullptr;
	}
	Super::DestroyObject();
}

