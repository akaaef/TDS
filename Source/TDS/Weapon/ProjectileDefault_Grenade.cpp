// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"

#include "DrawDebugHelpers.h"
#include "StaticMeshAttributes.h"
#include "TDS/Character/TDSCharacter.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}


void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//Super::BulletCollisionSphereHit(HitComp,OtherActor,OtherComp,NormalImpulse,Hit);
	
	Explose();
	
	Super::BulletCollisionSphereHit(HitComp,OtherActor,OtherComp,NormalImpulse,Hit);
	
}

void AProjectileDefault_Grenade::Explose()
{
	if(ProjectileSetting.ExploseFX)
	{
		ExploseFX_Multicast(ProjectileSetting.ExploseFX);
	}
	if(ProjectileSetting.ExploseSound)
	{
		ExploseSound_Multicast(ProjectileSetting.ExploseSound);
	}
	TArray<AActor*>IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage*0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		NULL, IgnoredActor,this,nullptr,ECC_Visibility);
	
	if(DebugRadius)
	{
		FVector Center = BulletMesh->GetComponentLocation();
		DrawDebugSphere(GetWorld(),Center,ProjectileSetting.ProjectileMinRadiusDamage,12,FColor::Red,false,5,0,0);
		DrawDebugSphere(GetWorld(),Center,ProjectileSetting.ProjectileMaxRadiusDamage/2,12,FColor::Yellow,false,5,0,0);
		DrawDebugSphere(GetWorld(),Center,ProjectileSetting.ProjectileMaxRadiusDamage,12,FColor::Green,false,5,0,0);
	}

	ATDSCharacter* Character = Cast<ATDSCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
	if (Character)
	{
		Character->ShakeCamera();
	}
	
	this->Destroy();
}

void AProjectileDefault_Grenade::ExploseSound_Multicast_Implementation(USoundBase* Sound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(),ProjectileSetting.ExploseSound,GetActorLocation());
}


void AProjectileDefault_Grenade::ExploseFX_Multicast_Implementation(UParticleSystem* FX)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),FX,GetActorLocation(),GetActorRotation(),FVector(1));
}
