// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapon/WeaponDefault.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*,Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd,bool,bIsSuccess,int32,AmmoSafe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFire,UAnimMontage*,Anim);
UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FFire onFire;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	
	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated,EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo WeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Tick func
	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void DropMagazinTick(float DeltaTime);
	void DropSleevaTick(float DeltaTime);
	
	void WeaponInit();
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool WeaponReloading = false;
	

	UFUNCTION(Server,Reliable,BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);
	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();
	

	void FinishReload();
	void Fire();
	void InitReload();
	void CancelReload();

	UFUNCTION(NetMulticast,Unreliable)
	void HitScan_Multicast(FHitResult HitResult);
	

	UFUNCTION(Server,Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh,FTransform OffSet,FVector DropImpulseDirection,float LifeTimeMesh,float ImpulseRandomDisersion,float PowerImpulse,float CustoMass);

	
	//Timers'flags
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	bool WeaponAiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FName IdWeaponName;



	//Remove Debug!!!!
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
	float ReloadTime = 0.0f;

	void ChangeDispersioByShot();
	float GetCurrentDispersion()const;
	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0.0f);
	
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;
	FVector GetFireEndLocation()const;
	UFUNCTION(Server,reliable)
	void UpdateStatWeapon_OnServer(EMovementState NewMovementState);

	//flagsT
	bool BlockFire = false;
	//Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;
	//DropSleeva
	bool DropSleevaFlag = false;
	float DropSleevaTimer = -1.0f;
	//DropMagazin
	bool DropMagazineFlag = false;
	float DropMagazineTimer = -1.0f;

	bool CheckCanWeaponReload();
	int8 GetAviableAmmoForReload();

	int8 GetNumberProjectileByShot()const;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;
	
	UFUNCTION(Server,Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation,bool NewShouldReduceDispersion);

	UFUNCTION(NetMulticast,Unreliable)
	void AnimWeaponStart_Multicast(UAnimMontage* Anim);
	UFUNCTION(NetMulticast,Unreliable)
	void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform OffSet, FVector DropImpulseDirection,
	float LifeTimeMesh, float ImpulseRandomDisersion, float PowerImpulse, float CustoMass,FVector LocalDir);

	UFUNCTION(NetMulticast,Unreliable)
	void FXWeaponFire_Multicast(UParticleSystem* FXFire,USoundBase* SoundFire);
	
};