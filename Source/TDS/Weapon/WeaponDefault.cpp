// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"

#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "DrawDebugHelpers.h"
#include "ProjectileDefault.h"
#include "TDSState_Effect.h"
#include "TDS/Character/TDSCharacter.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Character/TDSInventoryComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"


// Sets default values
AWeaponDefault::AWeaponDefault()
{

	SetReplicates(true);
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	
	
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	DropMagazinTick(DeltaTime);
	DropSleevaTick(DeltaTime);
}
void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
		{
			FireTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if(WeaponReloading)
	{
		if(ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if(!WeaponReloading)
	{
		if(!WeaponFiring)
		{
			if(ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}
		if(CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if(CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
}

void AWeaponDefault::DropMagazinTick(float DeltaTime)
{
	if(DropMagazineFlag)
	{
		if(DropMagazineTimer < 0.0f)
		{
			DropMagazineFlag = false;
			InitDropMesh_OnServer(WeaponSetting.DropMagazin.StaticMesh,WeaponSetting.DropMagazin.DropMeshOffSet,WeaponSetting.DropMagazin.DropMeshImpulseDir,WeaponSetting.DropMagazin.DropMeshLifeTime,WeaponSetting.DropMagazin.ImpulseRandomDispersion,WeaponSetting.DropMagazin.PowerImpulse,WeaponSetting.DropMagazin.CustomMass);
		}
		else
		{
			DropMagazineTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DropSleevaTick(float DeltaTime)
{
	if(DropSleevaFlag)
	{
		if(DropSleevaTimer < 0.0f)
		{
			DropSleevaFlag = false;
			InitDropMesh_OnServer(WeaponSetting.DropSleeva.StaticMesh,WeaponSetting.DropSleeva.DropMeshOffSet,WeaponSetting.DropSleeva.DropMeshImpulseDir,WeaponSetting.DropSleeva.DropMeshLifeTime,WeaponSetting.DropSleeva.ImpulseRandomDispersion,WeaponSetting.DropSleeva.PowerImpulse,WeaponSetting.DropSleeva.CustomMass);
		}
		else
		{
			DropSleevaTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
		FireTimer = 0.01f;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{	
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	
	int8 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - WeaponInfo.Round;

	if(NeedToReload > AviableAmmoFromInventory)
	{
		WeaponInfo.Round += AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		WeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}
	
	OnWeaponReloadEnd.Broadcast(true,-AmmoNeedTakeFromInv);
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(),WeaponSetting.SoundReloadWeaponEnd,ShootLocation->GetComponentLocation());
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	
	OnWeaponReloadEnd.Broadcast(false,0);
	DropMagazineFlag = false;
}


void AWeaponDefault::HitScan_Multicast_Implementation(FHitResult HitResult)
{
	FVector Start = ShootLocation->GetComponentLocation();
	FVector End = ShootLocation->GetForwardVector()*3000.0f + Start;
	GetWorld()->LineTraceSingleByChannel(HitResult,Start,End,ECC_WorldStatic);
	DrawDebugLine(GetWorld(),Start,End,FColor::Orange,false,5);
}


void AWeaponDefault::Fire()
{
	//On Server by WeaponFire bool
	
	ATDSCharacter* Character = Cast<ATDSCharacter>(GetOwner()); ///  ФИКС БАГА КОГДА ПОСЛЕ СМЕРТИ СТРЕЛЯЕТ ЧАРАКТЕР 
	if(Character->bIsAlive) ///  ФИКС БАГА КОГДА ПОСЛЕ СМЕРТИ СТРЕЛЯЕТ ЧАРАКТЕР 
	{
		UAnimMontage* AnimToPlay = nullptr;
		if(WeaponAiming)
		{
			AnimToPlay = WeaponSetting.AnimationInfo.AnimCharFireAim;
		}
		else
		{
			AnimToPlay = WeaponSetting.AnimationInfo.AnimCharFire;
		}
		if(WeaponSetting.AnimationInfo.WeaponFire)
		{
			AnimWeaponStart_Multicast(WeaponSetting.AnimationInfo.WeaponFire);
		}
		
		if(WeaponSetting.DropSleeva.StaticMesh)
		{
			if(WeaponSetting.DropSleeva.DropMeshTime < 0.0f)
			{
				InitDropMesh_OnServer(WeaponSetting.DropSleeva.StaticMesh,WeaponSetting.DropSleeva.DropMeshOffSet,WeaponSetting.DropSleeva.DropMeshImpulseDir,WeaponSetting.DropSleeva.DropMeshLifeTime,WeaponSetting.DropSleeva.ImpulseRandomDispersion,WeaponSetting.DropSleeva.PowerImpulse,WeaponSetting.DropSleeva.CustomMass);
			}
			else
			{
				DropSleevaFlag = true;
				DropSleevaTimer = WeaponSetting.DropSleeva.DropMeshTime;
			}
		}


		
		FireTimer = WeaponSetting.RateOfFire;
		WeaponInfo.Round = WeaponInfo.Round - 1;
		ChangeDispersioByShot();
		onFire.Broadcast(AnimToPlay);

		FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon,WeaponSetting.SoundFireWeapon);
		
		int8 NomberProjectile = GetNumberProjectileByShot();
		if (ShootLocation)
		{
			FVector SpawnLocation = ShootLocation->GetComponentLocation();
			FRotator SpawnRotation = ShootLocation->GetComponentRotation();
			FProjectileInfo ProjectileInfo;
			ProjectileInfo = GetProjectile();
			
			FVector EndLocation;
			
			for(int8 i = 0; i<NomberProjectile; i++)
			{
				EndLocation = GetFireEndLocation();

				if (ProjectileInfo.Projectile)
				{
					FVector Dir = EndLocation - SpawnLocation;
					Dir.Normalize();

					FMatrix myMatrix(Dir,FVector(0,1,0),FVector(0,0,1),FVector::ZeroVector);
					SpawnRotation = myMatrix.Rotator();

					FActorSpawnParameters SpawnParams;
					SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					SpawnParams.Owner = GetOwner();
					SpawnParams.Instigator = GetInstigator();
					

					AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
					if (myProjectile)
					{
						myProjectile->InitPojectile(WeaponSetting.ProjectileSetting);
					}
				}
				else
				{
					
					FHitResult Hit;
					TArray<AActor*> Actors;
					
					if (ShowDebug)
					{
						HitScan_Multicast(Hit);
						
					}
					
					UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors,EDrawDebugTrace::None, Hit, true, FLinearColor::Red,FLinearColor::Green, 5.0f);
					
					
					if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
					{
						EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
						
						if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
						{
							UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

							if (myMaterial && Hit.GetComponent())
							{
								UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
							}
						}
						if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
						{
							UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
							if (myParticle)
							{
								UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
							}
						}

						if (WeaponSetting.ProjectileSetting.HitSound)
						{
							UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
						}
						UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, Hit.TraceStart,Hit, GetInstigatorController(),this,NULL);
						
						//UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
						
						UTypes::AddEffectBySurfaceType(Hit.GetActor(),NAME_None,ProjectileInfo.Effect,mySurfacetype);
						
						/*if(Hit.GetActor()->GetClass()->ImplementsInterface(UTDSIGameActor::StaticClass()))
						{
							ITDSIGameActor::Execute_AviableForEffects(Hit.GetActor());
							ITDSIGameActor::Execute_AviableForEffectsBP(Hit.GetActor());
						}*/
						
						//UTDSState_Effect* NewEffect = NewObject<UTDSState_Effect>(Hit.GetActor(),FName("Effect"));
					}
				}
			}
		}
		if(GetWeaponRound() <= 0 && !WeaponReloading)
		{
			if(CheckCanWeaponReload())
			{
				InitReload();
			}
		}	
	}
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer =  WeaponSetting.ReloadTime;

	UAnimMontage* AnimToPlay = nullptr;
	if(WeaponAiming)
	{
		AnimToPlay = WeaponSetting.AnimationInfo.AnimCharReloadAim;
	}
	else
	{
		AnimToPlay = WeaponSetting.AnimationInfo.AnimCharReload;
	}

	OnWeaponReloadStart.Broadcast(AnimToPlay);

	UAnimMontage* AnimWeaponToPlay = nullptr;
	if(WeaponAiming)
	{
		AnimWeaponToPlay = WeaponSetting.AnimationInfo.WeaponReloadAim;
	}
	else
	{
		AnimWeaponToPlay = WeaponSetting.AnimationInfo.WeaponReload;
	}

	if(WeaponSetting.AnimationInfo.WeaponFire && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		//SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
		AnimWeaponStart_Multicast(AnimWeaponToPlay);
	}
	
	if(WeaponSetting.DropMagazin.StaticMesh)
	{
		DropMagazineFlag = true;
		DropMagazineTimer = WeaponSetting.DropMagazin.DropMeshTime;
	}
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(),WeaponSetting.SoundReloadWeapon,ShootLocation->GetComponentLocation());
}



void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform OffSet, FVector DropImpulseDirection,
	float LifeTimeMesh, float ImpulseRandomDisersion, float PowerImpulse, float CustoMass)
{
	if(DropMesh)
	{
		FTransform Transform;
		FVector LocalDir = this->GetActorForwardVector() * OffSet.GetLocation().X + this->GetActorRightVector() * OffSet.GetLocation().Y + this->GetActorUpVector() * OffSet.GetLocation().Z;
		Transform.SetLocation(GetActorLocation()+LocalDir);
		Transform.SetScale3D(OffSet.GetScale3D());

		Transform.SetRotation((GetActorRotation() + OffSet.Rotator()).Quaternion());
		
		ShellDropFire_Multicast(DropMesh,Transform,DropImpulseDirection,LifeTimeMesh,ImpulseRandomDisersion,PowerImpulse,CustoMass,LocalDir);
		
	}
}

void AWeaponDefault::ChangeDispersioByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation()-ShootEndLocation);
	if(tmpV.Size()>SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetComponentLocation()-ShootEndLocation).GetSafeNormal()* - 20000.0f;
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
	}
	return EndLocation;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if(GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType,AviableAmmoForWeapon))
			{
				result = false;
				MyInv->OnWeaponNotHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
			else
			{
				MyInv->OnWeaponHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
		}
	}
	return result;
}

int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int8 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if(GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType,AviableAmmoForWeapon))
			{
				
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FXFire, USoundBase* SoundFire)
{
	if(WeaponSetting.SoundFireWeapon)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(),SoundFire,ShootLocation->GetComponentLocation());
	}
	if(WeaponSetting.EffectFireWeapon)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),FXFire,ShootLocation->GetComponentTransform());
		//UGameplayStatics::SpawnEmitterAttached(FXFire,SkeletalMeshWeapon,FName("MuzzleFlash"));
	}
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh, FTransform OffSet, FVector DropImpulseDirection,
                                                            float LifeTimeMesh, float ImpulseRandomDisersion, float PowerImpulse, float CustoMass,FVector LocalDir)
{

	FActorSpawnParameters Param;
	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	AStaticMeshActor* NewActor = nullptr;
	
	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(),OffSet,Param);
	if(NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);


		NewActor->SetActorTickEnabled(false);
		NewActor->InitialLifeSpan = LifeTimeMesh;
			
		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			
		//NewActor->GetStaticMeshComponent()->SetCollisionObjectType()
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse:: ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if(CustoMass>0.0f)
		{
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg((NAME_None),CustoMass,true);
		}
			
		if(!DropImpulseDirection.IsNearlyZero())
		{
			FVector FinalDir;
			LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

				
			if(!FMath::IsNearlyZero(ImpulseRandomDisersion))
			{
				FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir,ImpulseRandomDisersion);
				FinalDir.GetSafeNormal(0.0001f);
				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir*PowerImpulse);
			}
		}
	}
}

void AWeaponDefault::AnimWeaponStart_Multicast_Implementation(UAnimMontage* Anim)
{
	if(Anim && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(Anim);
	}
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation,
                                                                                  bool NewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = NewShouldReduceDispersion;
}


void AWeaponDefault::UpdateStatWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	BlockFire = false;
	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		WeaponAiming = false;
		BlockFire = true;
		SetWeaponStateFire_OnServer(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AWeaponDefault,WeaponInfo);
	
}
