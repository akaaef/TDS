// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS/Weapon/TDSState_Effect.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDSIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDSIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ITDSIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	/*UFUNCTION(BlueprintCallable,BlueprintNativeEvent,Category = "Event")
	bool AviableForEffects();
	UFUNCTION(BlueprintCallable,BlueprintImplementableEvent,Category = "Event")
	void AviableForEffectsBP();*/

	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UTDSState_Effect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDSState_Effect* RemoveEffect);
	virtual void AddEffect(UTDSState_Effect* NewEffect);
	
	//Inv
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo,int32 Cout);
	

};
