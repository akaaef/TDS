// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MatineeCameraShake.h"
#include "TDSInventoryComponent.h"
#include "TDSHealthCharacterComponent.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "Windows/AllowWindowsPlatformTypes.h"
#include "TDS/Weapon/WeaponDefault.h"
#include "TDS/Interface/TDSIGameActor.h"
#include "TDS/Weapon/TDSState_Effect.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter,public ITDSIGameActor
{
	GENERATED_BODY()

public:
	ATDSCharacter();


	FTimerHandle TimerHandle_RagDollTimer;

	
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = Inventory,meta = (AllowPrivateAccess = "true"))
	UTDSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = Inventory,meta = (AllowPrivateAccess = "true"))
	UTDSHealthCharacterComponent* CharacterHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UMatineeCameraShake> ShakeBase;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();
	UDecalComponent* CurrentCursor = nullptr;
	
	//Movement
	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	FCharacterSpeed MovementInfo;

	UFUNCTION(BlueprintCallable,Category = "Movement")
	EMovementState GetMovementState();
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	bool SprintRunEnable = false;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	bool WalkEnable = false;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	bool AimEnable = false;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	bool Fatigue = false;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	bool bIsAlive = true;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	TArray<UAnimMontage*>DeadsAnim;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Ability")
	TSubclassOf<UTDSState_Effect> AbilityEffect;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "GodMode")
	bool GodMode = false;
	

	//Inputs
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;


	void ShakeCamera();

	//Effects
	TArray<UTDSState_Effect*> Effects;
	

	//Weapon
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;
	
	//Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IDWeaponName,FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void Fire(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess,int32 AmmoTake);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void Fire_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	bool GetIsAlive();

	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();

	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

	void DropCurentWeapon();
	// Ability Func
	void TryAbilityEnabled();

	template<int32 ID>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(ID);
	}
	

	//Interface
	EPhysicalSurface GetSurfaceType()override;
	TArray<UTDSState_Effect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDSState_Effect* RemoveEffect) override;
	void AddEffect(UTDSState_Effect* NewEffect) override;
	//Interface end

	


	
	UPROPERTY(BlueprintReadOnly,EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
	void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	// Пофиксить баг который наносит полный урон на любой дистанции Character'у



	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(Server,Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);

	UFUNCTION(NetMulticast,Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);


	UFUNCTION(Server,Reliable)
	void SetMovementState_OnServer(EMovementState NewState);

	UFUNCTION(NetMulticast,Reliable)
	void SetMovementState_Multicast(EMovementState NewState);
};