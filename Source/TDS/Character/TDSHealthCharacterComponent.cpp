// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDSHealthCharacterComponent.h"

//#include "InteractiveGizmo.h"

void UTDSHealthCharacterComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue*CoefDamage;
	
	if(Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		
		if (Shield < 0.0f)
		{
			//FX
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDSHealthCharacterComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSHealthCharacterComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	OnShieldChange.Broadcast(Shield,ChangeValue);
	
	if(Shield >= 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if(Shield <= 0.0f)
		{
			Shield = 0.0f;
		}
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer,this,&UTDSHealthCharacterComponent::CoolDownShieldEnd,CoolDownShieldRecoveryTime,false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldTRecoveryRateTimer);
	}
	
}

void UTDSHealthCharacterComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldTRecoveryRateTimer,this,&UTDSHealthCharacterComponent::RecoveryShield,ShieldRecoveryRate,true);
	}
}

void UTDSHealthCharacterComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if(tmp > 100.0f)
	{
		Shield = 100.0f;

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldTRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield,ShieldRecoveryValue);
}
