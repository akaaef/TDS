// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/Interface/TDSIGameActor.h"
#include "TDS/Weapon/TDSState_Effect.h"
#include "TDSEnvironmentStructure.generated.h"

UCLASS()
class TDS_API ATDSEnvironmentStructure : public AActor,public ITDSIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UTDSState_Effect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDSState_Effect* RemoveEffect) override;
	void AddEffect(UTDSState_Effect* NewEffect) override;

	//Effects
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Setting")
	TArray<UTDSState_Effect*> Effects;
};
