// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATDSEnvironmentStructure::ATDSEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDSEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDSEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if(myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if(myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTDSState_Effect*> ATDSEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDSEnvironmentStructure::RemoveEffect(UTDSState_Effect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDSEnvironmentStructure::AddEffect(UTDSState_Effect* NewEffect)
{
	Effects.Add(NewEffect);
}
