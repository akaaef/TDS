
#pragma once

#include "CoreMinimal.h"
#include "MyMatineeCameraShake.h"
#include "GameplayCameras/Public/MatineeCameraShake.h"
#include "MyMatineeCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UMymatineeCameraShake : public UMatineeCameraShake
{
	GENERATED_BODY()
	
public:
	
	UMymatineeCameraShake();
};
