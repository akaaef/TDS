// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TDS/Interface/TDSIGameActor.h"
#include "TDS/TDS.h"


void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor,FName NameBonesHit,TSubclassOf<UTDSState_Effect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	
	if(SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTDSState_Effect* myEffect = Cast<UTDSState_Effect>(AddEffectClass->GetDefaultObject());
		
		if(myEffect)
		{
			bool bIsHvePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHvePossibleSurface)
			{
				if(myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHvePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if(!myEffect->bIsStackabale)
					{
						int8 j = 0;
						TArray<UTDSState_Effect*> CurrentEffects;
						ITDSIGameActor* myInterface = Cast<ITDSIGameActor>(TakeEffectActor);
						if(myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}
						if(CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if(CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}
					if (bIsCanAddEffect)
					{
						UTDSState_Effect* NewEffect = NewObject<UTDSState_Effect>(TakeEffectActor,AddEffectClass);
						if(NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor,NameBonesHit);
						}
					}
				}
				i++;
			}
		}
	}
}
