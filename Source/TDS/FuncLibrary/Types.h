// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TDS/Weapon/TDSState_Effect.h"

#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	AimWalk_State UMETA(DisplayName = "AimWalk_State"),
	SprintRun_State UMETA(DisplayName = "SprintRun_State"),
	Fatigue_State UMETA(DisplayName = "Fatigue_State"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "ShotGun "),
	SniperRifleType UMETA(DisplayName = "SniperRifle"),
	PistolType UMETA(DisplayName = "Pistol"),
	RocketLauncherType UMETA(DisplayName = "RocketLauncher"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	float SprintRunSpeedRun = 800.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Movement")
	float Fatigue = 200.0f;
	
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Recoil");
	float DispersionAimStart = 0.5f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Recoil");
	float DispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Recoil");
	float DispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Recoil");
	float DispersionAimShootCoef = 1.0f;


	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Aim_StateDispersionReduction = .3f;

	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float AimWalk_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Recoil" )
	float Run_StateDispersionReduction = 0.1f;
};


USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	TSubclassOf<class AProjectileDefault>Projectile = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	UStaticMesh* StaticMesh = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	UParticleSystem* TrialFX = nullptr;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	FTransform ProjectileStaticMeshOffset = FTransform();
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	FTransform ProjectileTrailFXOffset = FTransform();
	
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	float ProjectileInitSpeed = 2000.0f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	TMap<TEnumAsByte<EPhysicalSurface>,UMaterialInstance*> HitDecals;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	USoundBase* HitSound = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	TMap<TEnumAsByte<EPhysicalSurface>,UParticleSystem*> HitFXs;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Effect");
	TSubclassOf<UTDSState_Effect> Effect = nullptr;
	
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	UParticleSystem* ExploseFX = nullptr;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	USoundBase* ExploseSound = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	int DamageFalloff = 5;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="ProjectileSetting");
	float ExploseMaxDamage = 40.0f;
};

USTRUCT(BlueprintType)
struct FAnimationInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Char");
	UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Char");
	UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Char");
	UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Char");
	UAnimMontage* AnimCharReloadAim = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Weapon");
	UAnimMontage* WeaponReload = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Weapon");
	UAnimMontage* WeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Weapon");
	UAnimMontage* WeaponFire = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	UStaticMesh* StaticMesh = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	float DropMeshTime = -1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	float DropMeshLifeTime = 5.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	FTransform DropMeshOffSet = FTransform();
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	FVector DropMeshImpulseDir = FVector();
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	float PowerImpulse = 0.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	float CustomMass = 0.0f;
	
};


USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Class")
	TSubclassOf<class AWeaponDefault>WeaponClass = nullptr;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="State");
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="State");
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="State");
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="State");
	int32 NumberProjectileByShot = 1;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Sound");
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Sound");
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Sound");
	USoundBase* SoundReloadWeaponEnd = nullptr;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="FX");
	UParticleSystem* EffectFireWeapon = nullptr;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Trace");
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Trace");
	float DistanceTrace = 2000.0f;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="HitEffect");
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile");
	FProjectileInfo ProjectileSetting;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Anim Info");
	FAnimationInfo AnimationInfo;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	FDropMeshInfo DropMagazin;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="DropMesh");
	FDropMeshInfo DropSleeva;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion");
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Inventory");
	float SwitchTimeToWeapon = 1.0f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Inventory");
	UTexture2D* WeaponIcon = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Inventory");
	EWeaponType WeaponType = EWeaponType::RocketLauncherType;
};


UCLASS()
class TDS_API UTypes : public  UBlueprintFunctionLibrary
{
	GENERATED_BODY()


public:
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor,FName NameBonesHit,TSubclassOf<UTDSState_Effect> AddEffectClass,EPhysicalSurface SurfaceType);

	
};


USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Weapon State")
	int32 Round = 0;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "WeaponSlots")
	FName NameItem;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "WeaponSlots")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "AmmoSlots")
	EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "AmmoSlots")
	int32 Cout = 100;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "AmmoSlots")
	int32 MaxCout = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "DropWeapon")
	UStaticMesh* StaticMeshWeapon = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "DropWeapon")
	USkeletalMesh* SkeletalMeshWeapon = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "DropWeapon")
	FWeaponSlot WeaponInfo;
};



